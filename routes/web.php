<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

Route::get('/inicio','cbt1lerma@principal')->name('inicio');
Route::get('/login','cbt1lerma@ingreso')->name('login');
Route::get('/instalaciones','cbt1lerma@instal');

//ORIENTADOR
Route::get('/orientador','cbt1lerma@orientador')->name('orientador');
Route::POST('guarda_orientador','cbt1lerma@guarda_orientador')->name('guarda_orientador');
Route::get('/edita_orientador/{id_orientador}','cbt1lerma@edita_orientador')->name('edita_orientador');
Route::POST('guarda_orienmod','cbt1lerma@guarda_orienmod')->name('guarda_orienmod');
Route::get('/borra_orientador/{id_orientador}','cbt1lerma@borra_orientador')->name('borra_orientador');
Route::get('reporte_orientador','cbt1lerma@reporte_orientador');

//DOCENTE
Route::get('/docente','cbt1lerma@docentecreate')->name('docente');
Route::POST('guarda_docente','cbt1lerma@guarda_docente')->name('guarda_docente');
Route::get('/edita_docente/{id_docente}','cbt1lerma@edita_docente')->name('edita_docente');
Route::POST('guarda_docentmod','cbt1lerma@guarda_docentmod')->name('guarda_docentmod');
Route::get('reporte_docente','cbt1lerma@reporte_docente');
Route::get('/borra_docente/{id_docente}','cbt1lerma@borra_docente')->name('borra_docente');

//ALUMNO
Route::get('/alumno','cbt1lerma@alumno')->name('alumno');
Route::POST('guarda_alumno','cbt1lerma@guarda_alumno')->name('guarda_alumno');
Route::get('reporte_alumno','cbt1lerma@reporte_alumno')->name('reporte_docente');
Route::get('/edita_alumno/{id_alumno}','cbt1lerma@edita_alumno')->name('edita_alumno');
Route::POST('guarda_alumod','cbt1lerma@guarda_alumod')->name('guarda_alumod');
Route::get('/borra_alumno/{id_alumno}','cbt1lerma@borra_alumno')->name('borra_alumno');
//GENERACION
Route::get('generacion','cbt1lerma@generacion');
Route::POST('guarda_generacion','cbt1lerma@guarda_generacion')->name('guarda_generacion');
Route::get('reporte_generacion','cbt1lerma@reporte_generacion');
Route::get('/edita_generacion/{id_generacion}','cbt1lerma@edita_generacion')->name('edita_generacion');
Route::POST('guarda_genmod','cbt1lerma@guarda_genmod')->name('guarda_genmod');
Route::get('/borra_generacion/{id_generacion}','cbt1lerma@borra_generacion')->name('borra_generacion');
//CARRERA
Route::get('/carrera','cbt1lerma@carreracreate')->name('carrera');
Route::POST('guarda_carrera','cbt1lerma@guarda_carrera')->name('guarda_carrera');
Route::get('reporte_carrera','cbt1lerma@reporte_carrera');
Route::get('/edita_carrera/{id_carrera}','cbt1lerma@edita_carrera')->name('edita_carrera');
Route::POST('guarda_carmod','cbt1lerma@guarda_carmod')->name('guarda_carmod');
Route::get('/borra_carrera/{id_carrera}','cbt1lerma@borra_carrera')->name('borra_carrera');

Route::get('/trayectoria','cbt1lerma@trayec');
Route::get('/mivision','cbt1lerma@mi_vision');
Route::get('/carreras','cbt1lerma@car');
Route::get('/contacto','cbt1lerma@contact');
Route::any('/contacto/email','contactosController@contact')->name('email');
//NUEVAS RUTAS PARA ALTA Y REPORTE DEL ALUMNO
Route::get('/altaAlumno','cbt1lerma@altaAlumno')->name('altaAlumno');
Route::POST('/guardaAlumnoDet','cbt1lerma@guardaAlumnoDet')->name('guardaAlumnoDet');
Route::get('/validaDocumentos','cbt1lerma@validaDocumentos')->name('validaDocumentos');
Route::get('/reporteArchivos','cbt1lerma@reporteArchivos')->name('reporteArchivos');
Route::get('/tablaArchivos','cbt1lerma@tablaArchivos')->name('tablaArchivos');
Route::get('archivo/{doc}' , 'cbt1lerma@downloadFile');
Route::get('/modocu','cbt1lerma@modocu')->name('modocu');
//////////////////////////////////////////////////////////////////////////////////////////////////
