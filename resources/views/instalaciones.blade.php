<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>CBT No.1 Lerma | UTVT</title>

  <!-- Bootstrap -->
  <link rel="shortcut icon" href="public/img/cbt.jpg">
  <link href="public/css2/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="public/css2/font-awesome.css">
  <link rel="stylesheet" href="public/css2/animate.css">
  <link href="public/css2/style.css" rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

</head>

<body>
  <div class="wrapper" id="wrapper">
    <header>
      <div class="banner row" id="banner">
        <div class="parallax text-center" style="background-image: url(public/img/ini.jpg);">

          <div class="parallax-pattern-overlay">
            <div class="container text-center" style="height:600px;padding-top:170px;">
              <a href="#"><img id="site-title" class=" wow fadeInDown" wow-data-delay="0.0s" wow-data-duration="0.9s" src="public/img/2.png" alt=""/></a>
              <h2 class="intro"><!--<a href="#">CBT No.1 Dr. Gustavo Baz Prada, Lerma</a></h2>-->

            </div>
          </div>
        </div>
      </div>
      <div class="menu">
        <div class="navbar-wrapper">
          <div class="container">
            <div class="navwrapper">
              <div class="navbar navbar-inverse navbar-static-top">
                <div class="container">
                  <div class="navArea">
                    <div class="navbar-collapse collapse">
                      <ul class="nav navbar-nav">
                        <li class="menuItem active"><a href="inicio">Inicio</a></li>
                        <li class="menuItem"><a href="mivision">Misión y Visión</a></li>
                        <li class="menuItem"><a href="carreras">Carreras</a></li>
                        <li class="menuItem"><a href="instalaciones">Instalaciones y Talleres</a></li>
                        <li class="menuItem"><a href="trayectoria">Trayectoría y Calendario</a></li>
                        <li class="menuItem"><a href="contacto">Contáctanos</a></li>
                        <li class="menuItem"><a href="login">Login</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!----------------------------------------Instalaciones----------------------------------------->
    <section class="aboutus" id="aboutus">
      <div class="container">
        <div class="heading text-center">
          <br>
          <h2>Instalaciones</h2>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="papers text-center">
              <img src="public/img/team/1.jpeg" alt="">
              <br/>

            </div>
            <div class="papers text-center">
              <img src="public/img/team/23.jpg" alt="">
              <br/>

            </div>
            <div class="papers text-center">
              <img src="public/img/team/38.jpg" alt="">
              <br/>

            </div>
          </div>
          <div class="col-md-6">
            <div class="papers text-center">
              <img src="public/img/team/16.jpg" alt="">
              <br/>
            </div>
            <div class="papers text-center">
              <img src="public/img/team/18.jpg" alt="">
              <br/>

            </div>
            <div class="papers text-center">
              <img src="public/img/team/39.jpg" alt="">
              <br/>

            </div>
          </div>
        </div>
      </div>
    </section>
    <!--------------------------------------Talleres--------------------------------------------->
    <section class="aboutus" id="aboutus">
      <div class="container">
        <div class="heading text-center">
          <h2>Talleres</h2>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="papers text-center">
              <img src="public/img/team/10.jpeg" alt=""><br/>

            </div>
          </div>
          <div class="col-md-6">
            <div class="papers text-center">
              <img src="public/img/team/24.jpg" alt=""><br/>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--------------------------------------------Laboratorios-------------------------------------->
    <section class="aboutus" id="aboutus">
      <div class="container">
        <div class="heading text-center">
          <h2>Laboratorios</h2>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="papers text-center">
              <img src="public/img/team/7.jpg" alt=""><br/>

            </div>
          </div>
          <div class="col-md-6">
            <div class="papers text-center">
              <img src="public/img/team/28.jpg" alt=""><br/>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!--pie de página-->
    <section class="footer" id="footer">
      <p class="text-center">
        <a href="#wrapper" class="gototop"><i class="fa fa-angle-double-up fa-2x"></i></a>
      </p>
      <div class="container">
        <p>Síguenos en nuestras redes sociales</p>
        <ul>
          <li><a href="https://twitter.com/CBT1_LERMA?s=08" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.facebook.com/cbtn1dgbplerma/?ref=br_rs" target="_blank"><i class="fa fa-facebook"></i></a></li>
        </ul>
        <p>&copy; ProOnliPc Theme. Todos los Derechos Reservados.</p>
        <div class="credits">

          <a href="inicio">Centro de Bachillerato Tecnológico No.1 Dr. Gustavo Baz Prada, Lerma | Clave: 15ECT0001H</a> | <a href="">UTVT</a>
        </div>
      </div>
    </section>


  </div>
  <script src="public/js/jquery.js"></script>
  <script src="public/js/modernizr.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="public/js/bootstrap.min.js"></script>
  <script src="public/js/menustick.js"></script>
  <script src="public/js/parallax.js"></script>
  <script src="public/js/easing.js"></script>
  <script src="public/js/wow.js"></script>
  <script src="public/js/smoothscroll.js"></script>
  <script src="public/js/masonry.js"></script>
  <script src="public/js/imgloaded.js"></script>
  <script src="public/js/classie.js"></script>
  <script src="public/js/colorfinder.js"></script>
  <script src="public/js/gridscroll.js"></script>
  <script src="public/js/contact.js"></script>
  <script src="public/js/common.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>
  <script>
    wow = new WOW({}).init();
  </script>
  <script type="text/javascript">
    jQuery(function($) {
      $(document).ready(function() {
        //enabling stickUp on the '.navbar-wrapper' class
        $('.navbar-wrapper').stickUp({
          parts: {
            0: 'banner',
            1: 'aboutus',
            2: 'specialties',
            3: 'gallery',
            4: 'feedback',
            5: 'contact'
          },
          itemClass: 'menuItem',
          itemHover: 'active',
          topMargin: 'auto'
        });
      });

      //Google Map
      var get_latitude = $('#google-map').data('latitude');
      var get_longitude = $('#google-map').data('longitude');

      function initialize_google_map() {
        var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
        var mapOptions = {
          zoom: 14,
          scrollwheel: false,
          center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize_google_map);
    });
  </script>
  <script src="public/contactform/contactform.js"></script>


</body>

</html>
