@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>TABLA ALUMNO</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{('inicio')}}">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Tablas</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Reporte Alumnos</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Registro de Alumnos</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th><center>Nombre</center></th>
                        <th><center>Primer Apellido</center></th>
                        <th><center>Segundo Apellido</center></th>
                        <th><center>Correo</center></th>
                        <th><center>Carrera</center></th>
                        <th><center>Grado</center></th>
                        <th><center>Grupo</center></th>
                        <th colspan="2" ><center>Acciones</center></th>
                    </tr>
                    </thead>
                    <tbody>
                @foreach($alumnos as $al)
                    <tr class="gradeC">
                        <td><center>{{$al->nombre}}</center></td>
                        <td><center>{{$al->ape_primero}}</center></td>
                        <td><center>{{$al->ape_segundo}}</center></td>
                        <td><center>{{$al->correo}}</center></td>
                        <td><center>{{$al->id_carrera}}</center></td>
                        <td><center>{{$al->id_grado}}</center></td>
                        <td><center>{{$al->id_grupo}}</center></td>
                        <td>
                            <a href="{{URL::action('cbt1lerma@edita_alumno', ['id_alumno'=> $al->id_alumno])}}">
                                <center><button class="btn btn-primary btn-sm" type="submit">Modificar</button></center>
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::action('cbt1lerma@borra_alumno', ['id_alumno'=>$al->id_alumno])}}">
                                <center><button class="btn btn-danger btn-sm" type="submit">Eliminar</button></center>
                            </a>
                        </td>
                    </tr>
                @endforeach
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
@stop
