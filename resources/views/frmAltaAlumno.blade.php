@extends('cabecera')

@section('contenido')
<!-- Mainly scripts -->
<script src="public/js/jquery-3.1.1.min.js"></script>
<script>
  $(document).ready(function(){   
        $('select').on('change', function() {
            var select = this.value ;
            var optName = $("#buscaAlumno").attr("name");                
            if(optName=="buscaAlumno"){                
                $('#validaAlumno').load('{{url('validaDocumentos')}}' + '?select=' + select);
            }            
                //setTimeout("$('#ImpLit').val($('#lbl').text());$('#lbl2').val($('#lbl').text());", 1000);                         
        });          
    });
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>FORMULARIO ALUMNO</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Inicio</a>
            </li>
            <li class="breadcrumb-item">
                <a>Formularios</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Formulario Alumno Nuevo</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Ingresa tus datos</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <!--<a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>-->
                </div>
            </div>
            <div class="ibox-content" id="content">
                 @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">{{session('success')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                  @endif
                  @if(session()->has('error'))
                    <div class="alert alert-danger" role="alert">{{session('error')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                  @endif
              {!! Form::open(['url' => 'guardaAlumnoDet', 'method' => 'POST' ,'files' => true]) !!}
            {{-- <!--    <form method="POST" action="{{route('guarda_alumno')}}" > --> --}}
                  {{csrf_field()}}
                  <div class="form-group row has-success">
                        <div class="col-sm-4">
                            <label for="nombreAlumno">Nombre del Alumno</label>
                            <select name="nombreAlumno" class="form-control" id="nombreAlumno">                                    
                                    @foreach($alumnos as $al)
                                    <option value= '{{$al->id_alumno}}'> {{$al->NomAlumno}}
                                    </option>
                                    @endforeach
                            </select>
                                <label for="matricula">Matricula</label>
                                <input type="text" name="matricula" id="matricula" class="form-control">
                            <label for="grupo">Grupo</label>
                            <select name="grupo" class="form-control" id="grupo">                                    
                                    @foreach($grupos as $gp)
                                    <option value= '{{$gp->id_grupo}}'> {{$gp->grupo}}
                                    </option>
                                    @endforeach
                            </select>
                            <label for="generacion">Generación</label>
                            <select name="generacion" class="form-control" id="generacion">                                    
                                    @foreach($generaciones as $gp)
                                    <option value= '{{$gp->id_generacion}}'> {{$gp->generacion}}
                                    </option>
                                    @endforeach
                            </select>
                            <label for="carrera">Carrera</label>
                            <select name="carrera" class="form-control" id="carrera">                                    
                                    @foreach($carreras as $gp)
                                    <option value= '{{$gp->id_carrera}}'> {{$gp->nom_carrera}}
                                    </option>
                                    @endforeach
                            </select>
                            <label for="cartaP">Carta de Presentación</label>
                            <input type="file" name="cartaP" accept=".pdf,.jpg,.png" multiple>
                            <label for="cartaA">Carta de Aceptación</label>
                            <input type="file" name="cartaA" accept=".pdf,.jpg,.png" multiple>
                            <label for="escalaE">Escala Evaluativa</label>
                            <input type="file" name="escalaE" accept=".pdf,.jpg,.png" multiple>
                            <label for="registroA">Registro de Aistencia</label>
                            <input type="file" name="registroA" accept=".pdf,.jpg,.png" multiple>
                            <label for="cartaT">Carta de Terminación</label>
                            <input type="file" name="cartaT" accept=".pdf,.jpg,.png" multiple>
                        </div>    
                        <div class="col-sm-8">
                                <label for="buscaAlumno">Busca Alumno</label>
                                <select name="buscaAlumno" class="form-control" id="buscaAlumno">                                    
                                        @foreach($alumnos as $al)
                                        <option value= '{{$al->id_alumno}}'> {{$al->NomAlumno}}
                                        </option>
                                        @endforeach
                                </select>
                                <div id="validaAlumno">
                                    
                                </div>
                        </div>                               
                  </div>
                  <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <center><button class="btn btn-danger btn-sm" type="submit">Cancelar</button>
                                        <button class="btn btn-primary btn-sm" type="submit">Guardar</button></center>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                {!! Form::close() !!}                                             
            </div>
        </div>
    </div>
</div>
</div>

@stop       
