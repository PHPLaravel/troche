<?php
$server = 'localhost';
  $username ='root';
  $password = '';
  $database = 'cbt1';

  try {
    $conn = new PDO("mysql:host=$server;dbname=$database;",$username, $password);
  } catch (PDOException $e) {
    die('Connection failed: '.$e->getMessage());
  }

  session_start();

  if(isset($_SESSION['user_id'])){
    header('');
  }

  //require 'database.php';

  if(!empty($_POST['email']) && !empty($_POST['password'])){
    $records = $conn->prepare('SELECT id, email, password FROM users WHERE email=:email');
    $records->bindParam(':email', $_POST['email']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $message = '';

    if(count($results) > 0 && password_verify($_POST['password'], $results['password'])){
      $_SESSION['user_id'] = $results['id'];
      header('');
    }
    else{
      $message = 'Lo siento, los datos no coinciden';
    }
  }
 ?>  

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CBT No.1 Lerma | UTVT</title>

    <link rel="shortcut icon" href="public/img/cbt.jpg">
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="public/css/animate.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet"> 

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">CBT1</h1>
            </div>
            <h3>Bienvenido</h3>
            <p></p>
            <p>Inicia Sesión</p>
            {!! Form::open(['route' => 'log.store', 'method' => 'POST']) !!}
                <div class="form-group">
                    <div class="col-sm-14">
                        {!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Correo Electrónico', 'required']); !!}
                    </div>
                </div>

                <div class="form-group">
                    
                    <div class="col-sm-14">
                        {!! Form::password('password',['class' => 'form-control', 'placeholder'=>'Contraseña', 'required']); !!}
                    </div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>

            {!! Form::close() !!}
            <p class="m-t"> <small>Centro de Bachillerato Tecnológico No.1 Dr. Gustavo Baz Prada, Lerma | Clave: 15ECT0001H | UTVT &copy; 2019</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="public/js/jquery-3.1.1.min.js"></script>
    <script src="public/js/popper.min.js"></script>
    <script src="public/js/bootstrap.js"></script>

</body>

</html>
