<table border="1" align="center" class="table table-dark table-hover table-responsive">
        <tr id="miTablaPersonalizada">
        <th><center>Alumno</center></th >
        <th><center>Carrera</center></td>
        <th><center>Generación</center></th>
        <th><center>Matricula</center></th >
        <th><center>Carta de Presentación</center></th >
        <th><center>Carta de Presentacion</center></th >
        <th><center>Escala Evaluativa</center></th >
        <th><center>Registro de Asistencia</center></th >
        <th><center>Carta de Terminación</center></th >
        </tr>
        @foreach($sql as $res)
            <tr>
                <th><center>{{$res->NomAlumno}}</center></th >
                <td><center>{{$res->nom_carrera}}</center></td>
                <td><center>{{$res->generacion}}</center></td>
                <td><center>{{$res->matricula}}</center></td>
                @if($res->cartaPresentacion=='sindocumento')
                <td><center><a href="#"><b></b></a><center></td>
                @else
                <td><center><a href="{{URL::action('cbt1lerma@downloadFile', ['doc'=>$res->cartaPresentacion])}}"><b>Administrar</b></a></center></td>
                @endif

                @if($res->cartaAceptacion=='sindocumento')
                <td><center><a href="#"><b></b></a></td>
                @else
                <td><center><a href="{{URL::action('cbt1lerma@downloadFile', ['doc'=>$res->cartaAceptacion])}}"><b>Administrar</b></a></center></td>
                @endif

                @if($res->escalaEvaluativa=='sindocumento')
                <td><center><a href="#"><b></b></a></td>
                @else
                <td><center><a href="{{URL::action('cbt1lerma@downloadFile', ['doc'=>$res->escalaEvaluativa])}}"><b>Administrar</b></a></center></td>
                @endif

                @if($res->registroAsistencia=='sindocumento')
                <td><center><a href="#"><b></b></a></td>
                @else
                <td><center><a href="{{URL::action('cbt1lerma@downloadFile', ['doc'=>$res->registroAsistencia])}}"><b>Administrar</b></a></center></td>
                @endif

                @if($res->cartaTerm=='sindocumento')
                <td><center><a href="#"><b></b></a></td>
                @else
                <td><center><a href="{{URL::action('cbt1lerma@downloadFile', ['doc'=>$res->cartaTerm])}}"><b>Administrar</b></a></center></td>
                @endif

            </tr>
        @endforeach
    </table>
