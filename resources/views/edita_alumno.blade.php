@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>FORMULARIO ALUMNO</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{asset('inicio')}}">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Formularios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Formulario Alumno</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ingresa tus datos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <!--<a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>-->
                            </div>
                        </div>
                        <div class="ibox-content">
                            {!! Form::open(['url' => 'guarda_alumod', 'method' => 'POST']) !!}
                      <!--      <form method="POST" action="{{route('guarda_alumod')}}" > -->
                                {{csrf_field()}}
                                <div class="form-group row has-success">
                                  {!! Form::label('id_alumno', 'Id', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('id_alumno'))
                                    <i> {{ $errors->first('id_alumno') }} </i>
                                    @endif
                                    <div class="col-sm-2">
                                        {!! Form::text('id_alumno', @$alumnos->id_alumno, ['class' => 'form-control', 'id' => 'id_alumno', 'readonly']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('nombre'))
                                    <i> {{ $errors->first('nombre') }} </i>
                                    @endif
                                    <div class="col-sm-8">
                                      {!! Form::text('nombre', @$alumnos->nombre, ['class' => 'form-control', 'id' => 'nombre', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('apeprimero', 'Primer Apellido', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('apeprimero'))
                                    <i> {{ $errors->first('apeprimero') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                      {!! Form::text('ape_primero', @$alumnos->ape_primero, ['class' => 'form-control', 'id' => 'ape_primero', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                    {!! Form::label('apesegundo', 'Segundo Apellido', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('apesegundo'))
                                    <i> {{ $errors->first('apesegundo') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                      {!! Form::text('ape_segundo', @$alumnos->ape_segundo, ['class' => 'form-control', 'id' => 'ape_segundo', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row has-success">
                                    {!! Form::label('edad', 'Edad', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('edad'))
                                    <i> {{ $errors->first('edad') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                        {!! Form::text('edad', @$alumnos->edad, ['class' => 'form-control', 'id' => 'edad', 'maxlength' => '2', 'minlength' => '2', 'required']); !!}
                                    </div>
                                    {!! Form::label('telefono', 'Teléfono', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('telefono'))
                                    <i> {{ $errors->first('telefono') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                      {!! Form::text('telefono', @$alumnos->telefono, ['class' => 'form-control', 'id' => 'telefono', 'maxlength' => '10', 'minlength' => '10', 'required']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('correo', 'Correo electrónico', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('correo'))
                                    <i> {{ $errors->first('correo') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                      {!! Form::email('correo', @$alumnos->correo, ['class' => 'form-control', 'id' => 'correo', 'required']); !!}
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row has-success">
                                    {!! Form::label('calle', 'Calle', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('calle'))
                                    <i> {{ $errors->first('calle') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                      {!! Form::text('calle', @$alumnos->calle, ['class' => 'form-control', 'id' => 'calle', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('colonia', 'Colonia', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('colonia'))
                                    <i> {{ $errors->first('colonia') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                      {!! Form::text('colonia', @$alumnos->colonia, ['class' => 'form-control', 'id' => 'colonia', 'required pattern' => '[Az-Za]']); !!}
                                </div>
                              </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('municipio', 'Municipio', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('municipio'))
                                    <i> {{ $errors->first('municipio') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                      {!! Form::text('municipio', @$alumnos->municipio, ['class' => 'form-control', 'id' => 'municipio', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="col-sm-10">
                                    <div>
                                        {!! Form::label('sexo', 'Sexo', ['class' => 'col-sm-2 col-form-label']); !!}
                                        <div class="i-checks"><label> <input type="radio" @if($alumnos->sexo == "F") checked="" @endif value="F" id="f" name="sexo"> <i></i> Femenino </label></div>
                                      <div class="i-checks"><label> <input type="radio" @if($alumnos->sexo == "M") checked="" @endif id="m" value="M" name="sexo"> <i></i> Masculino </label></div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                    <div class="form-group row has-success">
                                        {!! Form::label('secarrera', 'Selecciona la carrera', ['class' => 'col-sm-2 col-form-label']); !!}
                                        <div class="col-sm-4">  
                                            <select name = 'id_carrera' class="form-control m-b">
                                                <option value = '{{$id_carrera}}'>{{$nom_carrera}}</option>
                                                @foreach($carreras as $c)
                                                <option value = '{{$c->id_carrera}}'>{{$c->nom_carrera}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                        <div class="form-group row has-success">
                                            {!! Form::label('Selecciona el grado en curso', 'Selecciona el grado en curso', ['class' => 'col-sm-2 col-form-label']); !!}
                                            <div class="col-sm-4">
                                                <select name = 'id_grado' class="form-control m-b">
                                                    <option value = '{{$id_grado}}'>{{$num_grado}}</option>
                                                    @foreach($grados as $g)
                                                    <option value = '{{$g->id_grado}}'>{{$g->num_grado}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    <div class="hr-line-dashed"></div>
                                        <div class="form-group row has-success">
                                            {!! Form::label('Selecciona el grupo', 'Selecciona el grupo', ['class' => 'col-sm-2 col-form-label']); !!}
                                            <div class="col-sm-4">
                                                <select name = 'id_grupo' class="form-control m-b">
                                                    <option value = '{{$id_grupo}}'>{{$nom_grupo}}</option>
                                                    @foreach($grupos as $g)
                                                    <option value = '{{$g->id_grupo}}'>{{$g->nom_grupo}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        {!! Form::submit('Guardar', ['class'=>'btn btn-primary btn-s']) !!}
                                    <a href="{{asset('reporte_alumno')}}">{!! Form::button('Cancelar', ['class'=>'btn btn-danger btn-s']) !!}</a>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            {!! Form::close() !!}
                            <?php
                            $redirect = false;
                            if ($redirect==true) {
                                header('Location'.$redirect_page);
                            }
                            ob_end_flush();?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop
