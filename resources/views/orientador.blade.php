@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>FORMULARIO ORIENTADOR</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="inicio">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Formularios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Formulario Orientador</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ingresa tus datos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <!--<a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>-->
                            </div>
                        </div>
                        <div class="ibox-content">
                          {!! Form::open(['url' => 'guarda_orientador', 'method' => 'POST']) !!}
                          <!--<form method="POST" action="{{route('guarda_orientador')}}" >-->
                              {{csrf_field()}}
                              <div class="form-group row has-success">
                                {!! Form::label('nom', 'Nombre', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-8">
                                  {!! Form::text('nombre',null,['class'=>'form-control','id'=>'nombre', 'required pattern'=>'[Az-Za]']) !!}

                                  </div>
                              </div>
                              <div class="form-group row has-success">
                                  {!! Form::label('app', 'Primer apellido', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-4">
                                    {!! Form::text('ape_primero',null,['class'=>'form-control','id'=>'ape_primero', 'required pattern'=>'[Az-Za]']) !!}
                                  </div>
                                  {!! Form::label('aps', 'Segundo Apellido', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-4">
                                    {!! Form::text('ape_segundo',null,['class'=>'form-control','id'=>'ape_segundo', 'required pattern'=>'[Az-Za]']) !!}
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group row has-success">
                                  {!! Form::label('ed', 'Edad', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-4">
                                    {!! Form::text('edad',null,['class'=>'form-control','id'=>'edad','maxlength'=>2, 'minlength'=>2, 'required'=>'true']) !!}
                                  </div>
                                  {!! Form::label('tel', 'Teléfono', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-4">
                                    {!! Form::text('telefono',null,['class'=>'form-control','id'=>'telefono','maxlength'=>10, 'minlength'=>10, 'required'=>'true']) !!}
                                  </div>
                              </div>
                              <div class="form-group row has-success">
                                  {!! Form::label('ce', 'Correo electrónico', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-6">
                                   {!! Form::email('correo', null, array('class'=>'form-control', 'required'=>'true'))!!}
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group row has-success">
                                  {!! Form::label('ca', 'Calle', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-6">
                                  {!! Form::text('calle',null,['class'=>'form-control','id'=>'calle', 'required pattern'=>'[Az-Za]']) !!}
                                  </div>
                              </div>
                              <div class="form-group row has-success">
                                  {!! Form::label('col', 'Colonia', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-6">
                                    {!! Form::text('colonia',null,['class'=>'form-control','id'=>'colonia', 'required pattern'=>'[Az-Za]']) !!}
                                  </div>
                              </div>
                              <div class="form-group row has-success">
                                  {!! Form::label('mun', 'Municipio', ['class' => 'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-6">
                                    {!! Form::text('municipio',null,['class'=>'form-control','id'=>'municipio', 'required pattern'=>'[Az-Za]']) !!}
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="col-sm-10">
                                  <div>
                                      {!! Form::label('se', 'Sexo', ['class' => 'col-sm-2 col-form-label']) !!}
                                      <div class="i-checks"><label> <input type="radio" value="F" checked="" id="f" name="sexo"> <i></i> Femenino </label></div>
                                      <div class="i-checks"><label> <input type="radio" id="m" value="M" name="sexo"> <i></i> Masculino </label></div>
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                                  <div class="form-group row has-success">
                                      {!! Form::label('gru', 'Selecciona el grupo a cargo', ['class' => 'col-sm-2 col-form-label']) !!}
                                      <div class="col-sm-4">
                                          <select name = 'id_grupo' class="form-control m-b">
                                                @foreach($grupos as $gru)
                                                <option value = '{{$gru->id_grado}}'>{{$gru->nom_grupo}}</option>
                                                @endforeach
                                              </select>
                                      </div>
                                  </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row has-success">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        {!! Form::submit('Guardar', ['class'=>'btn btn-primary btn-s']) !!}
                                        {!! Form::reset('Cancelar', ['class'=>'btn btn-danger btn-s']) !!}
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                          {!! Form::close() !!}
<!--</form>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
