@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>FORMULARIO CARRERA</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="inicio">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Formularios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Formulario Carrera</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ingresa los datos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <!--<a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>-->
                            </div>
                        </div>
                    <div class="ibox-content">


                            <form method="POST" action="{{route('guarda_carmod')}}" >
                                {{csrf_field()}}
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">
                                    ID</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="id_carrera" name="id_carrera" value="{{$carreras->id_carrera}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('carre','Nombre de la carrera',['class'=>'col-sm-2 col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('nom_carrera',@$carreras->nom_carrera,['class'=>'form-control','id'=>'nom_carrera', 'required pattern'=>'[Az-Za]']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                    {!! Form::submit('Guardar', ['class'=>'btn btn-primary btn-s']) !!}
                                    <a href="{{asset('reporte_carrera')}}">{!! Form::button('Cancelar', ['class'=>'btn btn-danger btn-s']) !!}</a>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
