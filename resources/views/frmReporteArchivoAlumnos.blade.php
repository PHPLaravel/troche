@extends('cabecera')

@section('contenido')
<!-- Mainly scripts -->
<script src="public/js/jquery-3.1.1.min.js"></script>
<script>
  $(document).ready(function(){
        $('#buscar').on('click', function() {
           // var data = $(this).serialize();
           enviar();
             //   $('#reporte').load('{{url('tablaArchivos')}}' + data);
                //setTimeout("$('#ImpLit').val($('#lbl').text());$('#lbl2').val($('#lbl').text());", 1000);
        });
        const enviar = () => {
          //  alert("Entro en la función enviar");
            const datos = $("form").serialize();

            $.ajax({
                cache: false,
                type: "GET",
                url: "{{url('tablaArchivos')}}",
                data: datos,
                error: function(request, status, error)
                {
                alert("ocurrio un error "+request.responseText)
                },
                success: function(data)
                {
                    $('#reporte').append(data);
                }
            });
        }
    });
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>FORMULARIO ALUMNO</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Inicio</a>
            </li>
            <li class="breadcrumb-item">
                <a>Formularios</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Formulario Vinculación</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Ingresa tus datos</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <!--<a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>-->
                </div>
            </div>
            <div class="ibox-content" id="content">
                 @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">{{session('success')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                  @endif
                  @if(session()->has('error'))
                    <div class="alert alert-danger" role="alert">{{session('error')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                  @endif
            {{-- <!--    <form method="POST" action="{{route('guarda_alumno')}}" > --> --}}
                <form action="#">
                  {{csrf_field()}}
                  <div class="form-group row has-success">
                        <div class="col-sm-6">
                            <label for="nombreAlumno">Nombre del Alumno</label>
                            <input type="text" name="nombreAlumno" id="nombreAlumno" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label for="generacion">Generación</label>
                            <select name="generacion" class="form-control" id="generacion">
                                    @foreach($generaciones as $gp)
                                      <option value= '{{$gp->id_generacion}}'> {{$gp->generacion}}
                                      </option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="carrera">Carrera</label>
                            <select name="carrera" class="form-control" id="carrera">
                                    @foreach($carreras as $gp)
                                    <option value= '{{$gp->id_carrera}}'> {{$gp->nom_carrera}}
                                    </option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="hr-line-dashed"></div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2"><br>
                                    <center><button class="btn btn-primary btn-sm" id="buscar">Buscar</button></center>
                            </div>
                            </div>
                        <div class="hr-line-dashed"></div>
                        <div class="col-sm-12">
                        <div id="reporte">

                        </div>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>

@stop
