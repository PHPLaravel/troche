@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>TABLA ORIENTADOR</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{asset('inicio')}}">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Tablas</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Reporte orientador</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Registro de Orientadores</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th><center>Nombre</center></th>
                        <th><center>Primer Apellido</center></th>
                        <th><center>Segundo Apellido</center></th>
                        <th><center>Correo</center></th>
                        <th><center>Grupo a cargo</center></th>
                        <th colspan="2" ><center>Acciones</center></th>
                    </tr>
                    </thead>
                    @foreach($orientadores as $or)
                    <tbody>
                    <tr class="gradeC">
                            <td><center>{{$or->nombre}}</center></td>
                            <td><center>{{$or->ape_primero}}</center></td>
                            <td><center>{{$or->ape_segundo}}</center></td>
                            <td><center>{{$or->correo}}</center></td>
                            <td><center>{{$or->id_grupo}}</center></td>
                            <td>
                              <a href="{{URL::action('cbt1lerma@edita_orientador', ['id_orientador'=> $or->id_orientador])}}">
                                <center><button class="btn btn-primary btn-sm" type="submit">Modificar</button></center>
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::action('cbt1lerma@borra_orientador', ['id_orientador'=>$or->id_orientador])}}">
                                    <center><button class="btn btn-danger btn-sm">Eliminar</button></center>
                                </a>
                            </td>

                    </tr>
                    @endforeach
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
@stop
