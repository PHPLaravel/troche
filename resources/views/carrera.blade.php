@extends('cabecera')

@section('contenido')

<!-- desde aqui -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>FORMULARIO CARRERA</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Formularios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Formulario Carrera</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ingresa los datos de la Carrera</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <!--<a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>-->
                            </div>
                        </div>
                        <div class="ibox-content">
                          <form method="POST" action="{{route('guarda_carrera')}}" >
                              {{csrf_field()}}
                              <div class="form-group row has-success">
                                  {!! Form::label('carre','Nombre de la carrera',['class'=>'col-sm-2 col-form-label']) !!}
                                  <div class="col-sm-8">
                                      {!! Form::text('nom_carrera',null,['class'=>'form-control','id'=>'nom_carrera', 'required pattern'=>'[Az-Za]']) !!}
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group row">
                                  <div class="col-sm-4 col-sm-offset-2">
                                      {!! Form::submit('Guardar', ['class'=>'btn btn-primary btn-s']) !!}
                                        {!! Form::reset('Cancelar', ['class'=>'btn btn-danger btn-s']) !!}
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- hasta aqui -->
         <!-- aqui -->
@stop
