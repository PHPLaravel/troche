@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>FORMULARIO GENERACIÓN</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="inicio">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Formularios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Formulario Generación</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ingresa los datos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
        <div class="ibox-content">

                            {!! Form::open(['url' => 'guarda_genmod']) !!}
                           <!-- <form method="POST" action="{{route('guarda_genmod')}}" >-->
                                {{csrf_field()}}
                                <div class="form-group  row">
                                    {!! Form::label('id_generacion','Generacion',['class'=>'col-sm-2 col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('id_generacion',@$generaciones->id_generacion, ['class' => 'form-control', 'readonly'=>'true']) !!}
                                        
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('ini_generacion','Ingresa el año del inicio ciclo escolar',['class'=>'col-sm-2 col-form-label']) !!}

                                    <div class="col-sm-4">
                                    {!! Form::text('ini_generacion', @$generaciones->ini_generacion, ['class' => 'form-control', 'maxlength'=>4,'minlength'=>4, 'required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('fin_generacion','Ingresa el año en el que concluye el ciclo escolar',['class'=>'col-sm-2 col-form-label']) !!}
                                    </label>
                                    <div class="col-sm-4">
                                        {!! Form::text('fin_generacion', @$generaciones->fin_generacion, ['class' => 'form-control', 'maxlength'=>4,'minlength'=>4, 'required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        {!! Form::submit('Guardar', ['class'=>'btn btn-primary btn-s']) !!}
                                       <a href="{{asset('reporte_generacion')}}">{!!Form::button('Cancelar', ['class'=>'btn btn-danger btn-s'])!!}</a>

                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
