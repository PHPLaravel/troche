<h4>1.Carta de Presentacion </h4>
<div class="custom-control custom-checkbox mb-3">
    @if($sql[0]->cartaPresentacion=="sindocumento")
        <input type="checkbox" class="custom-control-input" id="customControlValidation1" >
        @else
        <input type="checkbox" class="custom-control-input" id="customControlValidation1" checked>
    @endif
        <label class="custom-control-label" for="customControlValidation1">Carta de Presentación</label>        
      </div>
      <div class="custom-control custom-checkbox mb-3">
            @if($sql[0]->cartaAceptacion=="sindocumento")
                <input type="checkbox" class="custom-control-input" id="customControlValidation2" >
                @else
                <input type="checkbox" class="custom-control-input" id="customControlValidation2" checked>
            @endif
                <label class="custom-control-label" for="customControlValidation2">Carta de Aceptación</label>        
        </div>
        <div class="custom-control custom-checkbox mb-3">
                @if($sql[0]->escalaEvaluativa=="sindocumento")
                    <input type="checkbox" class="custom-control-input" id="customControlValidation3" >
                    @else
                    <input type="checkbox" class="custom-control-input" id="customControlValidation3" checked>
                @endif
                    <label class="custom-control-label" for="customControlValidation3">Escala Evaluativa</label>        
        </div>
        <div class="custom-control custom-checkbox mb-3">
                @if($sql[0]->registroAsistencia=="sindocumento")
                    <input type="checkbox" class="custom-control-input" id="customControlValidation4" >
                    @else
                    <input type="checkbox" class="custom-control-input" id="customControlValidation4" checked>
                @endif
                    <label class="custom-control-label" for="customControlValidation4">Registro Asistencia</label>        
        </div>
        <div class="custom-control custom-checkbox mb-3">
                @if($sql[0]->cartaTerm=="sindocumento")
                    <input type="checkbox" class="custom-control-input" id="customControlValidation5" >
                    @else
                    <input type="checkbox" class="custom-control-input" id="customControlValidation5" checked>
                @endif
                    <label class="custom-control-label" for="customControlValidation5">Carta de Terminación</label>        
        </div>