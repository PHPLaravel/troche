@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>FORMULARIO ALUMNO</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Formularios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Formulario Generación</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ingresa tus datos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <!--<a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>-->
                            </div>
                        </div>
                        <div class="ibox-content">
                          {!! Form::open(['url' => 'guarda_generacion', 'method' => 'POST']) !!}
                        <!--    <form method="POST" action="{{route('guarda_alumno')}}" > -->
                              {{csrf_field()}}
                              <div class="form-group row has-success">
                                    {!! Form::label('ini_generacion','Ingresa el año del inicio ciclo escolar',['class'=>'col-sm-2 col-form-label']) !!}

                                    <div class="col-sm-4">
                                    {!! Form::text('ini_generacion',null, ['class' => 'form-control', 'maxlength'=>4,'minlength'=>4, 'required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('fin_generacion','Ingresa el año en el que concluye el ciclo escolar',['class'=>'col-sm-2 col-form-label']) !!}
                                    </label>
                                    <div class="col-sm-4">
                                        {!! Form::text('fin_generacion',null, ['class' => 'form-control', 'maxlength'=>4,'minlength'=>4, 'required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                      <button class="btn btn-primary btn-sm" type="submit">Guardar</button>
                                      <button class="btn btn-danger btn-sm" type="submit">Cancelar</button>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
