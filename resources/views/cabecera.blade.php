<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CBT1 | FORMULARIO</title>

    <link rel="shortcut icon" href="public/img/logo.jpg">
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="public/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="public/css/animate.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet">

    <link href="public/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('public/img/logo.jpg')}}">
    <link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/style.css')}}" rel="stylesheet">

    <link href="{{asset('public/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <img alt="image" class="rounded-circle" src="{{asset('public/img/a7.jpg')}}" width="65px" height="65px" />
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="block m-t-xs font-bold">Administrador</span>
                        </a>
                    </div>
                    <div class="logo-element">
                        CBT1
                    </div>
                </li>
                <li>
                    <a href="inicio"><i class="fa fa-th-large"></i> <span class="nav-label">Inicio</span> <span class="fa arrow"></span></a>
                </li> <!-- aqui -->
                <li class="active">
                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Altas</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li ><a href="{{asset('alumno')}}">Alumno</a></li>
                        <li><a href="{{asset('docente')}}">Docente</a></li>
                        <li><a href="{{asset('orientador')}}">Orientador</a></li>
                        <li><a href="{{asset('carrera')}}">Carrera</a></li>
                        <li><a href="{{asset('generacion')}}">Generacion</a></li>

                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Reportes</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{asset('reporte_alumno')}}">Reporte Alumno</a></li>
                        <li><a href="{{asset('reporte_docente')}}">Reporte Docente</a></li>
                        <li><a href="{{asset('reporte_orientador')}}">Reporte Orientador</a></li>
                        <li><a href="{{asset('reporte_carrera')}}">Reporte Carrera</a></li>
                        <li><a href="{{asset('reporte_generacion')}}">Reporte Generacion</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Vinculación</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li ><a href="{{asset('altaAlumno')}}">Alta De Documentos</a></li>
                        <li><a href="{{asset('reporteArchivos')}}">Reporte De Alumnos</a></li>
                        <li><a href="{{asset('modoc')}}">Modificar Documentos</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Centro de Bachillerato Tecnológico No.1 Dr. Gustavo Baz Prada, Lerma</span>
                </li>
                <li>
                    <a href="{{asset('inicio')}}">
                        <i class="fa fa-sign-out"></i> Salir
                    </a>
                </li>
            </ul>

        </nav>
      </div>
      <div id="contenido">

        @yield('contenido')

      </div>

      <div class="footer">
          <div class="float-center">
              <strong>Copyright</strong> Centro de Bachillerato Tecnológico No.1 Dr. Gustavo Baz Prada, Lerma | Clave: 15ECT0001H | UTVT &copy; 2019
          </div>

      </div>

      </div>
      </div>


          <!-- Mainly scripts -->
          <script src="{{asset('public/js/jquery-3.1.1.min.js')}}"></script>
          <script src="{{asset('public/js/popper.min.js')}}"></script>
          <script src="{{asset('public/js/bootstrap.js')}}"></script>
          <script src="{{asset('public/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
          <script src="{{asset('public/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

          <!-- Custom and plugin javascript -->
          <script src="{{asset('public/js/inspinia.js')}}"></script>
          <script src="{{asset('public/js/plugins/pace/pace.min.js')}}"></script>

          <!-- iCheck -->
          <script src="public/js/plugins/iCheck/icheck.min.js"></script>
              <script>
                  $(document).ready(function () {
                      $('.i-checks').iCheck({
                          checkboxClass: 'icheckbox_square-green',
                          radioClass: 'iradio_square-green',
                      });
                  });
              </script>
  </body>
</html>
