<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Register</title>
     <link rel="shortcut icon" href="public/img/cbt.jpg">
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="public/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="public/css/animate.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">IN+</h1>

            </div>
            <h3>Register to IN+</h3>
            <p>Create account to see it in action.</p>
            {!! Form::open(['url' => 'guarda_usuario', 'method' => 'POST']) !!}
                <div class="col-sm-14">
                        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Cargo']); !!}
                    </div>
                   <br> 
                <div class="col-sm-14">
                        {!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Correo Electrónico']); !!}
                    </div>
                    <br>
                <div class="col-sm-14">
                        {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Contraseña','required' => 'true']); !!}
                    </div>
                    <br>
                <button type="submit" class="btn btn-primary block full-width m-b">Registrar</button>

                <a class="btn btn-sm btn-white btn-block" href="ingreso">Ingresar</a>
            {!! Form::close() !!}                
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="public/js/jquery-3.1.1.min.js"></script>
    <script src="public/js/popper.min.js"></script>
    <script src="public/js/bootstrap.js"></script>
    <!-- iCheck -->
    <script src="public/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
</body>

</html>
