@extends('cabecera')

@section('contenido')

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>FORMULARIO DOCENTE</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{asset('inicio')}}">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Formularios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Formulario Docente</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ingresa tus datos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <!--<a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>-->
                            </div>
                        </div>
                        <div class="ibox-content">
                            {!! Form::open(['url' => 'guarda_docentmod', 'method' => 'POST']) !!}
                            <!-- <form method="POST" action="{{route('guarda_docentmod')}}" > -->
                                {{csrf_field()}}
                                <div class="form-group row has-success">
                                  {!! Form::label('id_docente', 'Id', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('id_docente'))
                                    <i> {{ $errors->first('id_docente') }} </i>
                                    @endif
                                    <div class="col-sm-2">
                                      {!! Form::text('id_docente', @$docentes->id_docente, ['class' => 'form-control', 'id' => 'id_docente', 'readonly']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('nombre'))
                                    <i> {{ $errors->first('nombre') }} </i>
                                    @endif
                                    <div class="col-sm-8">
                                      {!! Form::text('nombre', @$docentes->nombre, ['class' => 'form-control', 'id' => 'nombre', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('apeprimero', 'Primer Apellido', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('apeprimero'))
                                    <i> {{ $errors->first('apeprimero') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                        {!! Form::text('ape_primero', @$docentes->ape_primero, ['class' => 'form-control', 'id' => 'ape_primero', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                    {!! Form::label('apesegundo', 'Segundo Apellido', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('apesegundo'))
                                    <i> {{ $errors->first('apesegundo') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                        {!! Form::text('ape_segundo', @$docentes->ape_segundo, ['class' => 'form-control', 'id' => 'ape_segundo', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row has-success">
                                    {!! Form::label('edad', 'Edad', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('edad'))
                                    <i> {{ $errors->first('edad') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                        {!! Form::text('edad', @$docentes->edad, ['class' => 'form-control', 'id' => 'edad', 'maxlength' => '2', 'minlength' => '2', 'required']); !!}
                                    </div>
                                    {!! Form::label('telefono', 'Teléfono', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('telefono'))
                                    <i> {{ $errors->first('telefono') }} </i>
                                    @endif
                                    <div class="col-sm-4">
                                        {!! Form::text('telefono', @$docentes->telefono, ['class' => 'form-control', 'id' => 'telefono', 'maxlength' => '10', 'minlength' => '10', 'required']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('correo', 'Correo electrónico', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('correo'))
                                    <i> {{ $errors->first('correo') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                        {!! Form::email('correo', @$docentes->correo, ['class' => 'form-control', 'id' => 'correo', 'required']); !!}
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row has-success">
                                    {!! Form::label('calle', 'Calle', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('calle'))
                                    <i> {{ $errors->first('calle') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                        {!! Form::text('calle', @$docentes->calle, ['class' => 'form-control', 'id' => 'calle', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('colonia', 'Colonia', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('colonia'))
                                    <i> {{ $errors->first('colonia') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                        {!! Form::text('colonia', @$docentes->colonia, ['class' => 'form-control', 'id' => 'colonia', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="form-group row has-success">
                                    {!! Form::label('municipio', 'Municipio', ['class' => 'col-sm-2 col-form-label']); !!}
                                    @if($errors->first('municipio'))
                                    <i> {{ $errors->first('municipio') }} </i>
                                    @endif
                                    <div class="col-sm-6">
                                        {!! Form::text('municipio', @$docentes->municipio, ['class' => 'form-control', 'id' => 'municipio', 'required pattern' => '[Az-Za]']); !!}
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="col-sm-10">
                                    <div>
                                        {!! Form::label('sexo', 'Sexo', ['class' => 'col-sm-2 col-form-label']); !!}
                                        <div class="i-checks"><label> <input type="radio" @if($docentes->sexo == "F") checked="" @endif value="F" id="f" name="sexo"> <i></i> Femenino </label></div>
                                      <div class="i-checks"><label> <input type="radio" @if($docentes->sexo == "M") checked="" @endif id="m" value="M" name="sexo"> <i></i> Masculino </label></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Guardar</button>
                                        <a href="{{asset('reporte_docente')}}"><input class="btn btn-danger btn-sm" type="button" value="Cancelar"></a>
                                        
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
