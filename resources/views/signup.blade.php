<?php
$server = 'localhost';
  $username ='root';
  $password = '';
  $database = 'cbt1';

  try {
    $conn = new PDO("mysql:host=$server;dbname=$database;",$username, $password);
  } catch (PDOException $e) {
    die('Connection failed: '.$e->getMessage());
  }

  session_start();

  if(isset($_SESSION['user_id'])){
    header('');
  }
  //require 'database.php';

  $message = '';

  if(!empty($_POST['email'])  && !empty($_POST['password'])){
    $sql = "INSERT INTO users (email, password) VALUES (:email, :password)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':email', $_POST['email']);
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password);

    if($stmt->execute()){
      $message = "Usuario creado satisfactoriamente";
    }
    else{
      $message = "Ha ocurrido un error creando su contraseña";
    }
  }
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>REGISTRARSE</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
   <!-- <?php
     // require 'partials/header.php'
    ?> -->

    <?php
      if(!empty($message)):
     ?>
     <p><?= $message ?></p>
   <?php endif; ?>

    <h1>REGISTRATE</h1>
    <span>o <a href="login.blade.php">Iniciar Sesión</a>
    </span>
    <form action="guarda_registro" method="post">
      <input type="email" name="email" placeholder="Ingresa tu correo">
      <input type="password" name="password" placeholder="Ingresa tu contraseña">
      <input type="password" name="confirm_password" placeholder="Confirma tu contraseña">
      <input type="submit" value="Enviar">

    </form>
  </body>
</html>
