<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\orientadores;
use App\alumnos;
use App\grupos;
use App\docentes;
use App\grados;
use App\generaciones;
use App\carreras;
use Mail;
use Session;
use Redirect;
//use DB;

class cbt1lerma extends Controller
{
  ////////////////

    public function principal(){
      return view('inicio');
    }
    public function contact(){
      return view('contacto');
    }
    public function ingreso(){
      return view('login');
    }
    public function trayec(){
      return view('trayectoria');
    }
    public function mi_vision(){
      return view('mivision');
    }
    public function car(){
      return view('carreras');
    }
    public function instal(){
      return view('instalaciones');
    }

/////////////////////////////////////////////////////////////---ORIENTADOR---///////////////////////////////////////////////////////////////////

    public function orientador(){
      $grupos = grupos::all();
      return view('orientador')->with('grupos',$grupos);
    }

    public function guarda_orientador(Request $request){

          $nombre = $request->nombre;
          $ape_primero = $request->ape_primero;
          $ape_segundo = $request->ape_segundo;
          $edad = $request->edad;
          $sexo = $request->sexo;
          $telefono = $request->telefono;
          $correo = $request->correo;
          $calle = $request->calle;
          $colonia = $request->colonia;
          $municipio = $request->municipio;
          $id_grupo = $request->id_grupo;


          $orien = new orientadores;
          $orien->nombre = $request->nombre;
          $orien->ape_primero = $request->ape_primero;
          $orien->ape_segundo = $request->ape_segundo;
          $orien->edad = $request->edad;
          $orien->sexo = $request->sexo;
          $orien->telefono = $request->telefono;
          $orien->correo = $request->correo;
          $orien->calle = $request->calle;
          $orien->colonia = $request->colonia;
          $orien->municipio = $request->municipio;
          $orien->id_grupo = $request->id_grupo;
          $orien->save();

          echo"<script>alert('Datos guardados correctamente')</script>";
          $grupos = grupos::all();
          return view('orientador')->with('grupos',$grupos);
    }

    public function edita_orientador($id_orientador){

      $orientadores=orientadores::where('id_orientador',$id_orientador)->get();

      $id_grupo = $orientadores[0]->id_grupo;
      $nom_grupo = grupos::where('id_grupo',$id_grupo)->get();
      $grupos = grupos::where('id_grupo','<>',$id_grupo)->get();

      return view ('edita_orientador')
      ->with('orientadores',$orientadores[0])
      ->with('id_grupo',$id_grupo)
      ->with('nom_grupo',$nom_grupo[0]->nom_grupo)
      ->with('grupos',$grupos);
    }

    public function guarda_orienmod(Request $request){
  	$id_orientador = $request->id_orientador;
  	$nombre = $request->nombre;
    $ape_primero = $request->ape_primero;
    $ape_segundo = $request->ape_segundo;
    $edad = $request->edad;
    $sexo = $request->sexo;
    $telefono = $request->telefono;
    $correo = $request->correo;
    $calle = $request->calle;
    $colonia = $request->colonia;
    $municipio = $request->municipio;
    $id_grupo = $request->id_grupo;

    $orien = orientadores::find($id_orientador);
    $orien->nombre = $request->nombre;
    $orien->ape_primero = $request->ape_primero;
    $orien->ape_segundo = $request->ape_segundo;
    $orien->edad = $request->edad;
    $orien->sexo = $request->sexo;
    $orien->telefono = $request->telefono;
    $orien->correo = $request->correo;
    $orien->calle = $request->calle;
    $orien->colonia = $request->colonia;
    $orien->municipio = $request->municipio;
    $orien->id_grupo = $request->id_grupo;
    $orien->save();
    echo"<script>alert('Datos modificados correctamente')</script>";
    $orientadores = orientadores::all();
  	return view('reporte_orientador')->with('orientadores',$orientadores);
  }

    public function borra_orientador($id_orientador){

          orientadores::find($id_orientador)->delete();
          echo"<script>alert('$id_orientador fue eliminado con éxito')</script>";
          return view ('reporte_orientador');
    }

    public function reporte_orientador(){
      $orientadores = orientadores::all();
    	    return view('reporte_orientador')
          ->with('orientadores',$orientadores);
    }

/////////////////////////////////////////////////////////////---DOCENTE---///////////////////////////////////////////////////////////////////

    public function docentecreate(){
      return view('docente');
    }

    public function guarda_docente(Request $request){

          $nombre = $request->nombre;
          $ape_primero = $request->ape_primero;
          $ape_segundo = $request->ape_segundo;
          $edad = $request->edad;
          $sexo = $request->sexo;
          $telefono = $request->telefono;
          $correo = $request->correo;
          $calle = $request->calle;
          $colonia = $request->colonia;
          $municipio = $request->municipio;

          $docen = new docentes;
          $docen->nombre = $request->nombre;
          $docen->ape_primero = $request->ape_primero;
          $docen->ape_segundo = $request->ape_segundo;
          $docen->edad = $request->edad;
          $docen->sexo = $request->sexo;
          $docen->telefono = $request->telefono;
          $docen->correo = $request->correo;
          $docen->calle = $request->calle;
          $docen->colonia = $request->colonia;
          $docen->municipio = $request->municipio;
          $docen->save();

          echo"<script>alert('Datos guardados correctamente')</script>";
          return view('docente');
    }

    public function edita_docente($id_docente){
      $docentes=docentes::where('id_docente',$id_docente)->get();

      return view ('edita_docente')
      ->with('docentes',$docentes[0]);
    }
    public function guarda_docentmod(Request $request){
  	$id_docente = $request->id_docente;
    $nombre = $request->nombre;
    $ape_primero = $request->ape_primero;
    $ape_segundo = $request->ape_segundo;
    $edad = $request->edad;
    $sexo = $request->sexo;
    $telefono = $request->telefono;
    $correo = $request->correo;
    $calle = $request->calle;
    $colonia = $request->colonia;
    $municipio = $request->municipio;

    $docen = docentes::find($id_docente);
    $docen->nombre = $request->nombre;
    $docen->ape_primero = $request->ape_primero;
    $docen->ape_segundo = $request->ape_segundo;
    $docen->edad = $request->edad;
    $docen->sexo = $request->sexo;
    $docen->telefono = $request->telefono;
    $docen->correo = $request->correo;
    $docen->calle = $request->calle;
    $docen->colonia = $request->colonia;
    $docen->municipio = $request->municipio;
    $docen->save();

    echo"<script>alert('Datos modificados correctamente')</script>";
    $docentes = docentes::all();
  	return view('reporte_docente')->with('docentes',$docentes);
  }

    public function borra_docente($id_docente){
          docentes::find($id_docente)->delete();
          echo"<script>alert('$id_docente fue eliminado con éxito')</script>";
          return redirect()->route('reporte_docente');
    }

    public function reporte_docente(){
      $docentes = docentes::all();
    	return view('reporte_docente')
      ->with('docentes',$docentes);
    }

/////////////////////////////////////////////////////////////---ALUMNO---///////////////////////////////////////////////////////////////////

    public function alumno(){
      $carreras = carreras::all();
      $grados = grados::all();
      $grupos = grupos::all();
      return view('alumno')
      ->with('carreras',$carreras)
      ->with('grados',$grados)
      ->with('grupos',$grupos);
    }

    public function guarda_alumno(Request $request){

          $nombre = $request->nombre;
          $ape_primero = $request->ape_primero;
          $ape_segundo = $request->ape_segundo;
          $edad = $request->edad;
          $sexo = $request->sexo;
          $telefono = $request->telefono;
          $correo = $request->correo;
          $calle = $request->calle;
          $colonia = $request->colonia;
          $municipio = $request->municipio;
          $id_carrera = $request->id_carrera;
          $id_grado = $request->id_grado;
          $id_grupo = $request->id_grupo;

          $alumn = new alumnos;
          $alumn->nombre = $request->nombre;
          $alumn->ape_primero = $request->ape_primero;
          $alumn->ape_segundo = $request->ape_segundo;
          $alumn->edad = $request->edad;
          $alumn->sexo = $request->sexo;
          $alumn->telefono = $request->telefono;
          $alumn->correo = $request->correo;
          $alumn->calle = $request->calle;
          $alumn->colonia = $request->colonia;
          $alumn->municipio = $request->municipio;
          $alumn->id_carrera = $request->id_carrera;
          $alumn->id_grado = $request->id_grado;
          $alumn->id_grupo = $request->id_grupo;
          $alumn->save();

          echo"<script>alert('Datos guardados correctamente')</script>";
          $carreras = carreras::all();
          $grados = grados::all();
          $grupos = grupos::all();
          return view('alumno')
          ->with('carreras',$carreras)
          ->with('grados',$grados)
          ->with('grupos',$grupos);
    }
    public function reporte_alumno(){
    $alumnos = alumnos::all();
      return view('reporte_alumno')
      ->with('alumnos',$alumnos);
    }
    public function edita_alumno($id_alumno){

      $alumnos=alumnos::where('id_alumno',$id_alumno)->get();

      $id_carrera = $alumnos[0]->id_carrera;
      $nom_carrera = carreras::where('id_carrera',$id_carrera)->get();
      $carreras = carreras::where('id_carrera','<>',$id_carrera)->get();

      $id_grado = $alumnos[0]->id_grado;
      $num_grado = grados::where('id_grado',$id_grado)->get();
      $grados = grados::where('id_grado','<>',$id_grado)->get();

      $id_grupo = $alumnos[0]->id_grupo;
      $nom_grupo = grupos::where('id_grupo',$id_grupo)->get();
      $grupos = grupos::where('id_grupo','<>',$id_grupo)->get();

      return view ('edita_alumno')
      ->with('alumnos',$alumnos[0])
      ->with('id_carrera',$id_carrera)
      ->with('nom_carrera',$nom_carrera[0]->nom_carrera)
      ->with('carreras',$carreras)
      ->with('id_grado',$id_grado)
      ->with('num_grado',$num_grado[0]->num_grado)
      ->with('grados',$grados)
      ->with('id_grupo',$id_grupo)
      ->with('nom_grupo',$nom_grupo[0]->nom_grupo)
      ->with('grupos',$grupos);

    }

    public function guarda_alumod(Request $request){
      $id_alumno = $request->id_alumno;
      $nombre = $request->nombre;
      $ape_primero = $request->ape_primero;
      $ape_segundo = $request->ape_segundo;
      $edad = $request->edad;
      $sexo = $request->sexo;
      $telefono = $request->telefono;
      $correo = $request->correo;
      $calle = $request->calle;
      $colonia = $request->colonia;
      $municipio = $request->municipio;
      $id_carrera = $request->id_carrera;
      $id_grado = $request->id_grado;
      $id_grupo = $request->id_grupo;

      $alumn = alumnos::find($id_alumno);
      $alumn->nombre = $request->nombre;
      $alumn->ape_primero = $request->ape_primero;
      $alumn->ape_segundo = $request->ape_segundo;
      $alumn->edad = $request->edad;
      $alumn->sexo = $request->sexo;
      $alumn->telefono = $request->telefono;
      $alumn->correo = $request->correo;
      $alumn->calle = $request->calle;
      $alumn->colonia = $request->colonia;
      $alumn->municipio = $request->municipio;
      $alumn->id_carrera = $request->id_carrera;
      $alumn->id_grado = $request->id_grado;
      $alumn->id_grupo = $request->id_grupo;
      $alumn->save();

    echo"<script>alert('Datos modificados correctamente')</script>";
    $alumnos = alumnos::all();
    return view('reporte_alumno')->with('alumnos',$alumnos);
  }

    public function borra_alumno($id_alumno){

          alumnos::find($id_alumno)->delete();
          echo"<script>alert('$id_alumno fue eliminado con éxito')</script>";
          return view ('reporte_alumno');
    }

/////////////////////////////////////////////////////////////---CARRERA---///////////////////////////////////////////////////////////////////

    public function carreracreate(){
      return view('carrera');
    }

    public function guarda_carrera(Request $request){

          $nom_carrera = $request->nom_carrera;

          $carre = new carreras;
          $carre->nom_carrera = $request->nom_carrera;
          $carre->save();

          echo"<script>alert('Datos guardados correctamente')</script>";
          return view('carrera');
    }
    public function edita_carrera($id_carrera){
      $carreras=carreras::where('id_carrera',$id_carrera)->get();
      return view ('edita_carrera')
      ->with('carreras',$carreras[0]);
    }

    public function guarda_carmod(Request $request){
    $id_carrera = $request->id_carrera;
    $nom_carrera = $request->nom_carrera;

    $carre = carreras::find($id_carrera);
    $carre->nom_carrera = $request->nom_carrera;
    $carre->save();

    echo"<script>alert('Datos modificados correctamente')</script>";
    $carreras = carreras::all();
    return view('reporte_carrera')->with('carreras',$carreras);
  }

    public function borra_carrera($id_carrera){

          generaciones::find($id_carrera)->delete();
          echo"<script>alert('$id_carrera fue eliminada con éxito')</script>";
          return view ('reporte_carrera');
    }

    public function reporte_carrera(){
      $carreras = carreras::all();
      return view('reporte_carrera')->with('carreras',$carreras);
    }

/////////////////////////////////////////////////////////////---GENERACION---///////////////////////////////////////////////////////////////////

    public function generacion(){
        return view('generacion');
    }
    public function guarda_generacion(Request $request){

          $ini_generacion = $request->ini_generacion;
          $fin_generacion = $request->fin_generacion;

          $gen = new generaciones;
          $gen->ini_generacion = $request->ini_generacion;
          $gen->fin_generacion = $request->fin_generacion;
          $gen->save();

          echo"<script>alert('Datos guardados correctamente')</script>";
          return view('generacion');
    }

    public function edita_generacion($id_generacion){
      $generaciones=generaciones::where('id_generacion',$id_generacion)->get();
      return view ('edita_generacion')
      ->with('generaciones',$generaciones[0]);
    }

    public function guarda_genmod(Request $request){
    $id_generacion = $request->id_generacion;
    $ini_generacion = $request->ini_generacion;
    $fin_generacion = $request->fin_generacion;

    $orien = generaciones::find($id_generacion);
    $orien->ini_generacion = $request->ini_generacion;
    $orien->fin_generacion = $request->fin_generacion;
    $orien->save();

    echo"<script>alert('Datos modificados correctamente')</script>";
    $generaciones = generaciones::all();
      return view('reporte_generacion')->with('generaciones',$generaciones);
  }

    public function borra_generacion($id_generacion){

          generaciones::find($id_generacion)->delete();
          echo"<script>alert('$id_generacion fue eliminado con éxito')</script>";
          return view ('reporte_generacion');
    }

    public function reporte_generacion(){
      $generaciones = generaciones::all();
      return view('reporte_generacion')->with('generaciones',$generaciones);
    }
    //////////////////////////////////////////////---ALTA Y REPORTE DEL ALUMNO VINCULACION---////////////////////////////////////////////////////////////////
    public function altaAlumno(){
      $alumnos=\DB::select("SELECT id_alumno,CONCAT(nombre,' ',ape_primero,' ',ape_segundo)NomAlumno FROM alumnos");
      $grupos=\DB::select("SELECT id_grupo,grupo FROM grupos");
      $generaciones=\DB::select("SELECT * FROM generaciones");
      $carreras=\DB::select("SELECT * FROM carreras");
      return view('frmAltaAlumno')->with("alumnos",$alumnos)
            ->with("grupos",$grupos)->with("generaciones",$generaciones)->with("carreras",$carreras);
    }
    public function guardaAlumnoDet(Request $request){
      $i = 1;
      //OBTENEMOS LOS DOCUMENTOS QUE CARGAREMOS A LA BASE DE DATOS
      $file1 = $request->file('cartaP');
      $file2 = $request->file('cartaA');
      $file3 = $request->file('escalaE');
      $file4 = $request->file('registroA');
      $file5 = $request->file('cartaT');

      ///////////////////////////////////////////////
      //LA VARIABLE DOC ES LA QUE CONTENDRÁ EL NOMBRE QUE SE INSERTARÁ EN LA BASE DE DATOS
      $doc = "";
      // OBTENEMOS LOS DEMAS DATOS QUE SE INSERTARAN EN LA BASE DE DATOS
      $matricula = $request->matricula;
      $grupo = $request->grupo;
      $idAlumno = $request->nombreAlumno;
      $carrera = $request->carrera;
      $generacion = $request->generacion;
      $documentos = array();
      //EN TODOS LOS ARCHIVOS BUSCA SI ESTAN VACIOS O NO; SI ESTAN VACIOS LES PONE 'sindocumento'
      while($i <= 5){
        if(${"file$i"} != ""){
          $ldate = date('Ymd_His_');
          $ldate2 = date('Y-m-d');
          $doc = ${"file$i"}->getClientOriginalName();
          ${$doc.$i} = $ldate.$doc;
          \Storage::disk('local')->put(${$doc.$i}, \File::get(${"file$i"}));
        }
        else{
          ${$doc.$i} = 'sindocumento';
       }
       //METEMOS TODOS LOS NOMBRES DE LOS DOCUMENTOS EN UN ARREGLO PARA PODER DESPUES METERLOS EN LA BASE DE DATOS
       $documentos[] = ${$doc.$i} ;
       $i++;
      }
      $sql2 = DB::insert("INSERT INTO alumnosgrupos (id_alumno,id_grupo,matricula,cartaPresentacion,cartaAceptacion,
                            escalaEvaluativa,registroAsistencia,cartaTerm,id_carrera,id_generacion)
                            VALUES($idAlumno,$grupo,'$matricula','$documentos[0]','$documentos[1]','$documentos[2]','$documentos[3]',
                            '$documentos[4]',$carrera,$generacion)");
        if( $sql2 == 1){
          return back()->with('success','Datos Insertados Correctamente');
        }
        else{
          return back()->with('error','Datos Erroneos Favor de Verificar o Llame al Administrador');
        }
    }
    public function validaDocumentos(Request $request){
        $id = $request->select;
        $sql = \DB::select("SELECT cartaPresentacion,cartaAceptacion,escalaEvaluativa,registroAsistencia,
                          cartaTerm FROM alumnosgrupos where id_alumno=$id");
        return view('frmDetAltaAlumno')->with("sql",$sql);
    }
    public function reporteArchivos(Request $request){
      $generaciones=\DB::select("SELECT * FROM generaciones");
      $carreras=\DB::select("SELECT * FROM carreras");
      return view('frmReporteArchivoAlumnos')->with("carreras",$carreras)->with("generaciones",$generaciones);
    }
    public function tablaArchivos(Request $request){

     $id_carrera =  $request->carrera;
     $nombreAlumno = $request->nombreAlumno;
     $id_generacion = $request->generacion;
      $sql=\DB::select("SELECT t1.*,CONCAT(nombre,' ',ape_primero,' ',ape_segundo)NomAlumno,nom_carrera,generacion
                        FROM alumnosgrupos AS t1
                        INNER JOIN alumnos
                        ON t1.`id_alumno` = alumnos.id_alumno
                        INNER JOIN carreras
                        ON t1.`id_carrera` = carreras.id_carrera
                        INNER JOIN generaciones
                        ON t1.`id_generacion` = generaciones.id_generacion
                        WHERE t1.id_carrera = $id_carrera or t1.id_generacion=$id_generacion or alumnos.nombre like '%$nombreAlumno%'");
    return view('frmArchivosXAlumno')->with("sql",$sql)                       ;
    }
    public function downloadFile($doc){
      $pathtoFile = 'storage/archivo/'.$doc;
      return response()->download($pathtoFile);
    }

    public function modocu(Request $request){
      $alumnos=\DB::select("SELECT id_alumno,CONCAT(nombre,' ',ape_primero,' ',ape_segundo)NomAlumno FROM alumnos");
      $grupos=\DB::select("SELECT id_grupo,grupo FROM grupos");
      $generaciones=\DB::select("SELECT * FROM generaciones");
      $carreras=\DB::select("SELECT * FROM carreras");
            return view('modoc')->with("alumnos",$alumnos)
                  ->with("grupos",$grupos)->with("generaciones",$generaciones)->with("carreras",$carreras);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
