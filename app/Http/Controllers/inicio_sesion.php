<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;


class inicio_sesion extends Controller
{
    public function store(Request $request){
    	if (Auth::attempt(['email'=> $request['email'], 'password'=> $request['password']]))
    	{
    		return Redirect::to('alumno'); //ruta que lleva a admin
    	}
    	//Alerta de error
    		return Redirect::to('/ingreso'); //ruta que regresa a login

    }
}
