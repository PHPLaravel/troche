<table border="1" align="center" class="table table-dark table-hover table-responsive">
        <tr >
            <th>Alumno</th >
            <th>Grupo</td>
            <th>Carrera</th>
            <th>Generación</th >
            <th>Matricula</th>
            <th>Carta Presentación</th>
            <th>Carta Aceptación</th>
            <th>Escala Evaluativa</th>
            <th>Registro Asistencia</th>
            <th>Carta de Terminación</th>

        <tr>                
        @foreach($resultado as $res)
            <tr >            
            <th><img src="{{asset('archivo/'.$doctip)}}" alt="Tipo de Archivo" height="50" width="50"></th >
            <td>{{$res->NomDemandante}}</td>
            <td>{{$res->num_juicio}}</td>
            <td>{{$res->NomTipoJuicio}}</td>
            <td>{{$res->archivo}}</td>
            <td><a href="{{asset('archivo/'.$res->archivo)}}"><b>Descargar</b></a></td>
            </tr>
        @endforeach
    </table>