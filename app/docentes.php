<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class docentes extends Model
{
	public $timestamps = false;
    protected $table = 'docentes';
    protected $primaryKey = 'id_docente';
    protected $fillable = ['id_docente','nombre','ape_primero','ape_segundo','edad','sexo','telefono','correo','calle','colonia','municipio'];
}
