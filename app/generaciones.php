<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class generaciones extends Model
{
	public $timestamps = false;
    protected $table = 'generaciones';
    protected $primaryKey = 'id_generacion';
    protected $fillable = ['id_generacion','ini_generacion','fin_generacion'];
}
