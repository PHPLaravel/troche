<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumnos extends Model
{
	public $timestamps = false;
    protected $table = 'alumnos';
    protected $primaryKey = 'id_alumno';
    protected $fillable = ['id_alumno','nombre','ape_primero','ape_segundo','edad','sexo','telefono','correo','calle','colonia','municipio','id_carrera','id_grado','id_grupo'];
}
