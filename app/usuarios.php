<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumnos extends Model
{
    protected $table = 'alumnos';
    protected $primaryKey = 'id_alumno';
    protected $fillable = ['id_alumno','nombre','ape_primero','ape_segundo','edad','sexo','telefono','correo','calle','colonia','municipio','carrera','grado','grupo'];
}
