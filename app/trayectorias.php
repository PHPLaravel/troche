<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trayectorias extends Model
{
	public $timestamps = false;
    protected $table = 'trayectorias';
    protected $primaryKey = 'id_practica';
    protected $fillable = ['id_practica','prac_observacion','prac_ejec1','prac_ejec2','prac_ejec3','servicio_social','estadias','titulacion'];
}
