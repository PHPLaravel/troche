<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class correo extends Mailable
{
    use Queueable, SerializesModels;

    public $msj;
    public $subject="Comentario";//Asunto en el correo

    public function __construct($file)
    {
        $this->msj=$file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('contact');
    }
}
