<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class grupos extends Model
{
	public $timestamps = false;
    protected $table = 'grupos';
    protected $primaryKey = 'id_grupo';
    protected $fillable = ['id_grupo','nom_grupo'];
}
