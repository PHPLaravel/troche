<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class grados extends Model
{
	public $timestamps = false;
    protected $table = 'grados';
    protected $primaryKey = 'id_grado';
    protected $fillable = ['id_grado','num_grado'];
}
