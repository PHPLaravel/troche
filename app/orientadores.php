<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orientadores extends Model
{
	public $timestamps = false;
    protected $table = 'orientadores';
    protected $fillable = ['id_orientador','nombre','ape_primero','ape_segundo','edad','sexo','telefono','correo','calle','colonia','municipio', 'id_grupo'];
    protected $primaryKey = 'id_orientador';
}
